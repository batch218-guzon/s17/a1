/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getInfo() {
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");
		let fullName = firstName + lastName;
		let age = prompt("Enter your age: ");
		let location = prompt("Enter what city you live in: ");

		console.log("Hello, " + firstName + " " + lastName);
		console.log("You are " + age + " years old");
		console.log("You live in " + location);
	};

	getInfo();
/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function getFavBand() {
		let band1 = "Linkin Park"
		let band2 = "Panic at the Disco!";
		let band3 = "Bastille";
		let band4 = "One Ok Rock";
		let band5 = "Disturbed";

		
		console.log("1. " + band1);
		console.log("2. " + band2);
		console.log("3. " + band3);
		console.log("4. " + band4);
		console.log("5. " + band5);
	};

	getFavBand();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function getFavMovie() {
		let movie1 = "50 FIRST DATES";
		let movie1Rating = "65%";
		let movie2 = "Fate/Stay Night: Heaven's Feel";
		let movie2Rating = "79%";
		let movie3 = "RUROUNI KENSHIN: THE LEGEND ENDS" ;
		let movie3Rating = "81%";
		let movie4 = "SHERLOCK HOLMES: A GAME OF SHADOWS" ;
		let movie4Rating = "59%";
		let movie5 = "INCEPTION";
		let movie5Rating = "87%";

		console.log("1. " + movie1);
		console.log("Rotten Tomatoes Rating: " + movie1Rating);
		console.log("2. " + movie2);
		console.log("Rotten Tomatoes Rating: " + movie2Rating);
		console.log("3. " + movie3);
		console.log("Rotten Tomatoes Rating: " + movie3Rating);
		console.log("4. " + movie4);
		console.log("Rotten Tomatoes Rating: " + movie4Rating);
		console.log("5. " + movie5);
		console.log("Rotten Tomatoes Rating: " + movie5Rating);

	};

	getFavMovie();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);

printFriends();